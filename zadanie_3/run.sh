#!/usr/bin/env bash

cd RestApi

set -e

composer install -q

php bin/console doctrine:migrations:migrate

# create temp database
cp tests/test_data.db tests/temp_data.db

# make tests
vendor/bin/simple-phpunit || true

echo "# TESTS #"
# delete temp database
rm tests/temp_data.db

if [ $? -ne 0 ]; then
    echo "# TESTS FAIL - NOT RUN #"
    exit 1
fi

echo "# CODE SNIFFER #"
vendor/bin/phpcbf --standard=PSR2 -p src/Entity src/Controller tests || true
vendor/bin/phpcs --standard=PSR2 -p src/Entity src/Controller tests || true

echo "# RUN SERVER #"
php bin/console server:run
