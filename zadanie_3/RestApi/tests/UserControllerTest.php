<?php
/**
 * Created by PhpStorm.
 * User: maksym
 * Date: 20.05.19
 * Time: 16:37
 */

namespace App\Tests\Controller;

use App\Controller\UserController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{

    private $firstname = "Firstname";
    private $surname = "Surname";

    private $emptyValues = [false, array(), "0", 0, 0.0, "", null];
    private $varTypes = [true, "string", 1, 1.1];

    public function testClassUserPostSuccessAndGetOutput()
    {

        $controler = new UserController();
        $succesMsg = $controler->makeJsonMsg(
            201,
            'Success'
        );

        $dataArray = [];

        // POST method test
        $generatedIdentificationNumber = $this->generateIdentificationNumber();
        array_push(
            $dataArray,
            array(
                'firstname' => $this->firstname,
                'surname' => $this->surname,
                'identificationNumber' => $generatedIdentificationNumber
            )
        );

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $generatedIdentificationNumber
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals($succesMsg, $client->getResponse()->getContent());

        $generatedIdentificationNumber = $this->generateIdentificationNumber();
        array_push(
            $dataArray,
            array(
                'firstname' => $this->firstname,
                'surname' => $this->surname,
                'identificationNumber' => $generatedIdentificationNumber
            )
        );
        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $generatedIdentificationNumber
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals($succesMsg, $client->getResponse()->getContent());

        $generatedIdentificationNumber = $this->generateIdentificationNumber();
        array_push(
            $dataArray,
            array(
                'firstname' => $this->firstname,
                'surname' => $this->surname,
                'identificationNumber' => $generatedIdentificationNumber
            )
        );
        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $generatedIdentificationNumber
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals($succesMsg, $client->getResponse()->getContent());

        // GET method test
        $jsonData = json_encode($dataArray);

        $client = static::createClient();
        $client->request('GET', 'users', array(), array(), array(), null);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals($jsonData, $client->getResponse()->getContent());
    }

    public function testClassUserControllerPostValidiationFirstnameEmptyValues()
    {
        $controler = new UserController();
        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('firstname' => 'Invalid value for firstname')
        );

        $client = $this->postRequest(
            $this->emptyValues[0],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->emptyValues[1],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->emptyValues[2],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->emptyValues[3],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->emptyValues[4],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->emptyValues[5],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->emptyValues[6],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }


    public function testClassUserControllerPostValidiationFirstnameWrongType()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('firstname' => 'Invalid value for firstname')
        );

        $client = $this->postRequest(
            $this->varTypes[0],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->varTypes[2],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->varTypes[3],
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostValidiationSurnameEmptyValues()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('surname' => 'Invalid value for surname')
        );

        $client = $this->postRequest(
            $this->firstname,
            $this->emptyValues[0],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->emptyValues[1],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->emptyValues[2],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->emptyValues[3],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->emptyValues[4],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->emptyValues[5],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->emptyValues[6],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostValidiationSurnameWrongType()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('surname' => 'Invalid value for surname')
        );

        $client = $this->postRequest(
            $this->firstname,
            $this->varTypes[0],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->varTypes[2],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->varTypes[3],
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostValidiationIdentificationNumberEmptyValues()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('identificationNumber' => 'Invalid value for identificationNumber')
        );

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->emptyValues[0]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->emptyValues[1]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->emptyValues[2]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->emptyValues[3]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->emptyValues[4]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->emptyValues[5]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->emptyValues[6]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostValidiationIdentificationNumberWrongType()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('identificationNumber' => 'Invalid value for identificationNumber')
        );

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->varTypes[0]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->varTypes[1]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $this->varTypes[3]
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostValidiationFirstnameWrongData()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('firstname' => 'Invalid value for firstname')
        );

        // string < 3 characters
        $client = $this->postRequest(
            'st',
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        // string > 30 characters
        $client = $this->postRequest(
            'stringstringstringstringstringstring',
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        // string with whitespaces and others wrong characters
        $client = $this->postRequest(
            'string string !@#$%^&*()|/?<>.,":;',
            $this->surname,
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostValidiationSurnameWrongData()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('surname' => 'Invalid value for surname')
        );

        // string < 3 characters

        $client = $this->postRequest(
            $this->firstname,
            'st',
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        // string > 30 characters
        $client = $this->postRequest(
            $this->firstname,
            'stringstringstringstringstringstring',
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        // string with whitespaces and others wrong characters
        $client = $this->postRequest(
            $this->firstname,
            'string string !@#$%^&*()|/?<>.,":;',
            $this->generateIdentificationNumber()
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostValidiationIdentificationNumberWrongData()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('identificationNumber' => 'Invalid value for identificationNumber')
        );

        // int < 11 characters
        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            1
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);

        // int > 11 characters
        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            1111111111111
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostValidiationIdentificationNumberUnique()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('identificationNumber' => 'identificationNumber alerdy exist')
        );

        $generatedIdentificationNumber = $this->generateIdentificationNumber();

        // first POST - add new user
        $this->postRequest(
            $this->firstname,
            $this->surname,
            $generatedIdentificationNumber
        );

        // this same POST -> error
        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $generatedIdentificationNumber
        );

        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals($errorResponse, $client->getResponse()->getContent());
    }

    public function testClassUserControllerPostValidiationIdentificationNumberChecksum()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('identificationNumber' => 'Invalid checksum for identificationNumber')
        );

        $wrongIdentificationNumber = 10101010101;

        $client = $this->postRequest(
            $this->firstname,
            $this->surname,
            $wrongIdentificationNumber
        );
        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostEmptyAndInvalidRequest()
    {
        $controler = new UserController();

        $errorResponse = $controler->makeJsonMsg(
            400,
            'JSON Failed',
            array('jsonRequest' => 'Invalid JSON format')
        );

        $client = static::createClient();

        $client->request('POST', 'users', array(), array(), array(), null);

        //$jsonResponse = str_replace(array("[","]"),"", $client->getResponse()->getContent());
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals($errorResponse, $client->getResponse()->getContent());

        $client->request('POST', 'users', array(), array(), array(), "{,}");

        //$jsonResponse = str_replace(array("[","]"),"", $client->getResponse()->getContent());
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals($errorResponse, $client->getResponse()->getContent());

        $client->request('POST', 'users', array(), array(), array(), null);

        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);
        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostWithoutValues()
    {
        $controler = new UserController();
        $client = static::createClient();

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('firstname' => 'Invalid value for firstname')
        );

        $data = array(
            'surname' => $this->surname,
            'identificationNumber' => $this->generateIdentificationNumber()
        );

        $client->request('POST', 'users', array(), array(), array(), json_encode($data));

        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);

        $this->assertEquals($errorResponse, $jsonResponse);

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('surname' => 'Invalid value for surname')
        );

        $data = array(
            'firstname' => $this->firstname,
            'identificationNumber' => $this->generateIdentificationNumber()
        );

        $client->request('POST', 'users', array(), array(), array(), json_encode($data));

        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);

        $this->assertEquals($errorResponse, $jsonResponse);

        $errorResponse = $controler->makeJsonMsg(
            422,
            'Validation Failed',
            array('identificationNumber' => 'Invalid value for identificationNumber')
        );

        $data = array(
            'surname' => $this->surname,
            'firstname' => $this->firstname
        );

        $client->request('POST', 'users', array(), array(), array(), json_encode($data));

        $jsonResponse = str_replace(array("[","]"), "", $client->getResponse()->getContent());
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
        $this->assertJson($jsonResponse);

        $this->assertEquals($errorResponse, $jsonResponse);
    }

    public function testClassUserControllerPostPolishCharacters()
    {
        $controler = new UserController();
        $succesMsg = $controler->makeJsonMsg(
            201,
            'Success'
        );

        $client = $this->postRequest(
            'żźćńółęąś',
            'ŻŹĆĄŚĘŁÓŃ',
            $this->generateIdentificationNumber()
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals($succesMsg, $client->getResponse()->getContent());
    }

    /*
     * Support functions
     */

    /**
     * Performs POST request and returned Client
     *
     * @param $firstname
     * @param $surname
     * @param $identificationNumber
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    private function postRequest($firstname, $surname, $identificationNumber)
    {
        $client = static::createClient();

        $data = array(
            'firstname' => $firstname,
            'surname' => $surname,
            'identificationNumber' => $identificationNumber
        );

        $client->request('POST', 'users', array(), array(), array(), (string)json_encode($data));

        return $client;
    }

    /**
     * Generate Identification Number for tests
     *
     * @return int
     */
    private function generateIdentificationNumber()
    {

        $weights = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
        $length = count($weights);

        $fullYear = (int) date('Y');
        $year = (int) date('y');
        $month = date('m') + (((int) ($fullYear/100) - 14) % 5) * 20;
        $day = date('d');

        $result = array((int) ($year / 10), $year % 10, (int) ($month / 10), $month % 10, (int) ($day / 10), $day % 10);
        for ($i = 6; $i < $length; $i++) {
            $result[$i] = mt_rand(0, 9);
        }
        $result[$length - 1] |= 1;

        $checksum = 0;
        for ($i = 0; $i < $length; $i++) {
            $checksum += $weights[$i] * $result[$i];
        }
        $checksum = (10 - ($checksum % 10)) % 10;
        $result[] = $checksum;

        $result = implode('', $result);

        return (int) $result;
    }
}
