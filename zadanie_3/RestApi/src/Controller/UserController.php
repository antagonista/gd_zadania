<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractController
{

    /**
     * Show all users
     *
     * @Route("/users", methods={"GET"})
     *
     * @return object JsonResponse
     */
    public function getMethodUsers()
    {
        $allUsers = $this->getDoctrine()->getRepository(User::class)->findAll();
        $serializedEntity = $this->get('serializer')->serialize($allUsers, 'json', array('groups' => array('user')));

        return new JsonResponse($serializedEntity, 200, [], true);
    }


    /**
     * Add new user
     *
     * @Route("/users", methods={"POST"})
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return object JsonResponse
     */
    public function postMethodUsers(Request $request, ValidatorInterface $validator)
    {

        // Validiation JSON format
        $jsonDecode = json_decode($request->getContent(), true);

        if (is_null($jsonDecode)) {
            return new JsonResponse($this->makeJsonMsg(
                400,
                'JSON Failed',
                array('jsonRequest' => 'Invalid JSON format')
            ), 400, [], true);
        }

        // Validiation null values
        $errorArray = [];

        foreach ($jsonDecode as $name => $value) {
            if ($value == null) {
                $errorArray[$name] = 'Invalid value for ' . $name;
            }
        }

        if (!empty($errorArray)) {
            return new JsonResponse($this->makeJsonMsg(
                422,
                'Validation Failed',
                $errorArray
            ), 422, [], true);
        }

        // Basic validiation identificationNumber
        if (isset($jsonDecode['identificationNumber']) and !is_int($jsonDecode['identificationNumber'])) {
            return new JsonResponse($this->makeJsonMsg(
                422,
                'Validation Failed',
                array('identificationNumber' => 'Invalid value for identificationNumber')
            ), 422, [], true);
        }

        unset($jsonDecode);

        // Deserialize data
        $user = $this
            ->get('serializer')
            ->deserialize($request->getContent(), User::class, 'json');

        // Validiation data asserts
        $errorArray = $validator->validate($user);

        if (count($errorArray) > 0) {
            $errorMsg = [];

            foreach ($errorArray as $error) {
                $invalidProperty = $error->getPropertyPath();
                $errorMsg = array_merge(
                    $errorMsg,
                    array($invalidProperty =>$error->getMessage())
                );
            }

            return new JsonResponse($this->makeJsonMsg(
                422,
                'Validation Failed',
                $errorMsg
            ), 422, [], true);
        }

        // Validiation checksum identificationNumber
        if ($user->validiationIdentificationNumber() == false) {
            return new JsonResponse($this->makeJsonMsg(
                422,
                'Validation Failed',
                array('identificationNumber' => 'Invalid checksum for identificationNumber')
            ), 422, [], true);
        }

        // Set first character capitalized
        $user->setFirstname(ucfirst($user->getFirstname()));
        $user->setSurname(ucfirst($user->getSurname()));

        // Save data
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse($this->makeJsonMsg(
            201,
            'Success'
        ), 201, [], true);
    }

    /**
     * Create JSON response message
     *
     * @param $code
     * @param $message
     * @param null $errorArray
     * @return string
     */
    public function makeJsonMsg(
        $code,
        $message,
        $errorArray = null
    ) {

        $returnMsg = array('code' => $code, 'message' => $message);
        if ($errorArray != null) {
            $returnMsg['errors'] = $errorArray;
        }

        return json_encode($returnMsg);
    }
}
