<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *     "identificationNumber",
 *     message="identificationNumber alerdy exist")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message = "Invalid value for firstname")
     * @Assert\NotNull(message = "Invalid value for firstname")
     * @Assert\Type("string")
     * @Assert\Regex(
     *     pattern = "/^[A-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]{3,30}+$/",
     *     message = "Invalid value for firstname"
     * )
     *
     * @Groups({"user"})
     *
     * @ORM\Column(
     *     name="firstname",
     *     type="string",
     *     length=30)
     */
    private $firstname;

    /**
     * @Assert\NotBlank(message = "Invalid value for surname")
     * @Assert\NotNull(message = "Invalid value for surname")
     * @Assert\Type("string")
     * @Assert\Regex(
     *     pattern = "/^[A-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]{3,30}+$/",
     *     message = "Invalid value for surname"
     * )
     *
     * @Groups({"user"})
     *
     * @ORM\Column(
     *     name="surname",
     *     type="string",
     *     length=30)
     */
    private $surname;

    /**
     * @Assert\NotBlank(message = "Invalid value for identificationNumber")
     * @Assert\NotNull(message = "Invalid value for identificationNumber")
     * @Assert\Type("integer")
     * @Assert\Regex(
     *     pattern="/^[0-9]{11}+$/",
     *     message="Invalid value for identificationNumber"
     * )
     *
     * @Groups({"user"})
     *
     * @ORM\Column(
     *     name="identificationNumber",
     *     type="integer",
     *     unique=true)
     */
    private $identificationNumber;

    /**
     * Veryfication checksum Identification Number
     *
     * @return bool
     */
    public function validiationIdentificationNumber()
    {

        $identificationNumber = strval($this->identificationNumber);

        $arrSteps = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
        $intSum = 0;

        for ($i = 0; $i < 10; $i++) {
            $intSum += $arrSteps[$i] * $identificationNumber[$i];
        }

        $int = 10 - $intSum % 10;
        $intControlNr = ($int == 10) ? 0 : $int;

        if ($intControlNr == $identificationNumber[10]) {
            return true;
        }

        return false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getIdentificationNumber(): ?int
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber(int $identificationNumber): self
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }
}
