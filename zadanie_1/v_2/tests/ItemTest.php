<?php
/**
 * Created by PhpStorm.
 * User: maksym
 * Date: 17.05.19
 * Time: 16:07
 */

namespace DealerGroup\Tests;

use DealerGroup\Item;
use DealerGroup\Product;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{

    private $productName = "string";
    private $productPrice = 1.1;
    private $testObject;
    private $productObject;

    public function setUp()
    {
        $this->productObject = new Product($this->productName, $this->productPrice);
        $this->testObject = new Item($this->productObject, 1, 1);
    }

    /*
     * Class Construct Argument Quantity Tests
     */

    public function testClassConstructArgumentQuantityEmpty()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, null);
    }

    public function testClassConstructArgumentQuantityEmpty1()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, 0);
    }

    public function testClassConstructArgumentQuantityEmpty2()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, 0.0);
    }

    public function testClassConstructArgumentQuantityEmpty3()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, "");
    }

    public function testClassConstructArgumentQuantityEmpty4()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, "0");
    }

    public function testClassConstructArgumentQuantityEmpty5()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, array());
    }

    public function testClassConstructArgumentQuantityEmpty6()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, false);
    }

    public function testClassConstructArgumentQuantityFormat()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, 1.1);
    }

    public function testClassConstructArgumentQuantityFormat1()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, true);
    }

    public function testClassConstructArgumentQuantityFormat2()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, "string");
    }

    public function testClassConstructArgumentQuantityFormat3()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, array(1));
    }

    public function testClassConstructArgumentQuantityFormat4()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productObject, $this->testObject);
    }

    public function testClassConstructArgumentQuantityLogicalCondition()
    {
        $this->expectExceptionMessage('Item -> quantity must be > 0');
        new Item($this->productObject, -1);
    }

    /*
     * Class Method Tests
     */

    public function testClassMethodsQuantityAndReturnedFormat()
    {
        $item = new Item($this->productObject, 1, 1);

        $this->assertIsInt($item->getQuantity());
        $this->assertEquals($item->getQuantity(), 1);

        $this->assertIsBool($item->addQuantity(1));
        $this->assertIsInt($item->getQuantity());
        $this->assertEquals($item->getQuantity(), 2);

        $item = new Item($this->productObject, 5, 5);

        $this->assertIsInt($item->getQuantity());
        $this->assertEquals($item->getQuantity(), 5);

        $this->assertEquals($item->addQuantity(5), true);

        $this->assertIsInt($item->getQuantity());
        $this->assertEquals($item->getQuantity(), 10);
    }

    public function testClassGettersMethodsProductNameAndReturnedFormat()
    {
        $item = new Item($this->productObject, 1);

        $this->assertIsString($item->getProductName());
        $this->assertEquals($item->getProductName(), $this->productName);

        $this->assertIsFloat($item->getProductPrice());
        $this->assertEquals($item->getProductPrice(), $this->productPrice);

        $this->assertIsInt($item->getProductMinimalAmount());
        $this->assertEquals($item->getProductMinimalAmount(), 1);
    }

    public function testClassMethodTotalValueAndReturnedFormat()
    {
        $item = new Item($this->productObject, 1, 1);

        $this->assertIsFloat($item->getValue());
        $this->assertEquals($item->getValue(), $this->productPrice*1);

        $this->assertEquals($item->addQuantity(1), true);

        $this->assertIsFloat($item->getValue());
        $this->assertEquals($item->getValue(), $this->productPrice*2);

        $this->assertEquals($item->addQuantity(8), true);

        $this->assertIsFloat($item->getValue());
        $this->assertEquals($item->getValue(), $this->productPrice*10);
    }
}
