<?php
/**
 * Created by PhpStorm.
 * User: maksym
 * Date: 17.05.19
 * Time: 17:07
 */

namespace DealerGroup\Tests;

use DealerGroup\Cart;
use DealerGroup\Item;
use DealerGroup\Product;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CartTest extends TestCase
{

    public function testClassItemMethods()
    {

        $cart = new Cart();

        $this->assertIsBool($cart->deleteItem(""));
        $this->assertFalse($cart->deleteItem(""));

        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 0);

        $product = new Product("Product 1", 1);
        $item = new Item($product, 1); //1
        $this->assertEquals($cart->addItem($item), true);

        $product = new Product("Product 2", 2, 2);
        $item = new Item($product, 2); //4
        $this->assertEquals($cart->addItem($item), true);

        $product = new Product("Product 3", 3);
        $item = new Item($product, 3); //9
        $this->assertEquals($cart->addItem($item), true);

        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 1+4+9);

        // Not added new item - only quatity
        $product = new Product("Product 1", 1.5);
        $item = new Item($product, 1); //2
        $this->assertEquals($cart->addItem($item), true);

        $product = new Product("Product 2", 2.5);
        $item = new Item($product, 2); //8
        $this->assertEquals($cart->addItem($item), true);

        $product = new Product("Product 3", 3.5);
        $item = new Item($product, 3); //18
        $this->assertEquals($cart->addItem($item), true);

        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 2+8+18);

        // Delete Item
        $this->assertEquals($cart->deleteItem("Product 1"), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 8+18);

        // Delete not Exist Item
        $this->assertEquals($cart->deleteItem("Product 1"), false);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 8+18);

        // Add again this same Item after delete
        $product = new Product("Product 1", 1.5);
        $item = new Item($product, 1); // 1.5
        $this->assertEquals($cart->addItem($item), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 1.5+8+18);

        $item = new Item($product, 1); // 3
        $this->assertEquals($cart->addItem($item), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 3+8+18);

        $item = new Item($product, 2); // 6
        $this->assertEquals($cart->addItem($item), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 6+8+18);

        // Delete items before not deleted
        $this->assertEquals($cart->deleteItem("Product 2"), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 6+18);

        $this->assertEquals($cart->deleteItem("Product 3"), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 6);

        // Add product never before added
        $product = new Product("New Product", 99.99);
        $item = new Item($product, 1); // 99.99
        $this->assertEquals($cart->addItem($item), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 6+99.99);

        $product = new Product("New Product 2", 100000);
        $item = new Item($product, 6); // 600000
        $this->assertEquals($cart->addItem($item), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 6+99.99+600000);

        $product = new Product("New Product", 1.5);
        $item = new Item($product, 1); // 199.98
        $this->assertEquals($cart->addItem($item), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 6+199.98+600000);

        $product = new Product("New Product", 1.5);
        $item = new Item($product, 98); // 9999
        $this->assertEquals($cart->addItem($item), true);
        $this->assertIsFloat($cart->getTotalValue());
        $this->assertEquals($cart->getTotalValue(), 6+9999+600000);
    }

    public function testClassMethodArgumentDeleteItemEmpty()
    {
        $cart = new Cart();

        $this->assertFalse($cart->deleteItem(""));
        $this->assertFalse($cart->deleteItem("0"));
        $this->assertFalse($cart->deleteItem(0));
        $this->assertFalse($cart->deleteItem(0.));
        $this->assertFalse($cart->deleteItem(0.0));
        $this->assertFalse($cart->deleteItem(false));
        $this->assertFalse($cart->deleteItem(null));
        $this->assertFalse($cart->deleteItem(array()));

        // Test deletes after add item
        $product = new Product("Product", 1.1);
        $item = new Item($product, 1);
        $cart->addItem($item);

        $this->assertFalse($cart->deleteItem(""));
        $this->assertFalse($cart->deleteItem("0"));
        $this->assertFalse($cart->deleteItem(0));
        $this->assertFalse($cart->deleteItem(0.));
        $this->assertFalse($cart->deleteItem(0.0));
        $this->assertFalse($cart->deleteItem(false));
        $this->assertFalse($cart->deleteItem(null));
        $this->assertFalse($cart->deleteItem(array()));
    }
}
