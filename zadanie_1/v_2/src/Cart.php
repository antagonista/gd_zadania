<?php

namespace DealerGroup;

use DealerGroup\Item;

/**
 * Class Cart
 * @package DealerGroup
 */
class Cart
{

    /**
     * @var array $itemList Storage list of item object
     */
    private $itemList = array();

    /**
     * Add new item or increases the amount exist object in item list
     *
     * @param \DealerGroup\Item $item
     * @return bool
     */
    public function addItem(Item $item)
    {
        $itemExist = $this->existItem($item->getProductName());

        if ($itemExist === false) {
            // Add new item to storage list
            array_push($this->itemList, $item);
            return true;
        } else {
            // Add quantity to exist item in storage list
            $storageItem = $this->itemList[$itemExist];
            $storageItem->addQuantity($item->getQuantity());
            $this->itemList[$itemExist] = $storageItem;
            return true;
        }
    }

    /**
     * Delete item from item list by product name
     *
     * @param string $productName
     * @return bool
     */
    public function deleteItem($productName)
    {
        if (!empty($productName) and is_string($productName)) {
            $itemKey = $this->existItem($productName);

            if (is_int($itemKey)) {
                // item exist - delete item from storage list
                unset($this->itemList[$itemKey]);
                $this->itemList = array_values($this->itemList);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Returns total value of cart
     *
     * @return float
     */
    public function getTotalValue()
    {
        $totalValue = 0;

        foreach ($this->itemList as $item) {
            $totalValue += $item->getValue();
        }

        return (float) $totalValue;
    }

    /**
     * Checks whether the object exist in item list
     * If exist - return index an object from the item list
     * If not exist - return false
     *
     * @param $productName
     * @return bool|int
     */
    private function existItem($productName)
    {
        $count = 0;
        foreach ($this->itemList as $storageItem) {
            if ($storageItem->getProductName() == $productName) {
                // Item exist - return key of storaged item
                return $count;
                break;
            }
            $count++;
        }
        return false;
    }
}
