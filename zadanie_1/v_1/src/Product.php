<?php
/**
 * Created by PhpStorm.
 * User: maksym
 * Date: 17.05.19
 * Time: 12:17
 */

namespace DealerGroup;

/**
 * Class Product
 * @package DealerGroup
 */
class Product
{

    /**
     * @var string|null $name Product name
     */
    private $name = null;
    /**
     * @var float|null $price Product price
     */
    private $price = null;
    /**
     * @var integer $minimalAmount Product minimal amount
     */
    private $minimalAmount = 1;

    /**
     * Product constructor.
     * @param string $name
     * @param float $price
     * @param null|integer $minimalAmount
     */
    public function __construct($name, $price, $minimalAmount = null)
    {
        $this->setName($name);
        $this->setPrice($price);
        if (!is_null($minimalAmount)) {
            $this->setMinimalAmount($minimalAmount);
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    private function setName($name)
    {
        if (!empty($name) and is_string($name)) {
                $this->name = $name;
        } else {
            throw new \Exception('Product -> Invalid name argument');
        }
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    private function setPrice($price)
    {
        if (!empty($price)
            and (is_float($price) or is_int($price))
            and $price > 0) {
            if (is_int($price)) {
                $this->price = (float) $price;
            } else {
                // Check prince float format
                if (preg_match('/^[0-9]+\.[0-9]{0,2}$/', $price)) {
                    $this->price = (float) $price;
                } else {
                    throw new \Exception('Product -> price is wrong float format');
                }
            }
        } else {
            throw new \Exception('Product -> Invalid price argument');
        }
    }

    /**
     * @return integer
     */
    public function getMinimalAmount()
    {
        return $this->minimalAmount;
    }

    /**
     * @param integer $minimalAmount
     */
    private function setMinimalAmount($minimalAmount)
    {
        if (!empty($minimalAmount) and is_int($minimalAmount)) {
            if ($minimalAmount >= 1) {
                $this->minimalAmount = $minimalAmount;
            } else {
                throw new \Exception('Product -> minimal amount is not >= 1');
            }
        } else {
            throw new \Exception('Product -> Invalud minimal amount argument');
        }
    }
}
