<?php

namespace DealerGroup;

/**
 * Class Item
 * @package DealerGroup
 */
class Item extends Product
{

    /**
     * @var int $quantity Amount of product
     */
    private $quantity = 0;

    /**
     * Item constructor.
     *
     * @param string $productName
     * @param float $productPrice
     * @param integer $quantity
     * @param integer|null $minimalAmount
     */
    public function __construct($productName, $productPrice, $quantity, $minimalAmount = null)
    {
        parent::__construct($productName, $productPrice, $minimalAmount);
        $this->setQuantity($quantity);
    }

    /**
     * @param integer $quantity
     */
    private function setQuantity($quantity)
    {
        if ($this->validiationQuantity($quantity)) {
            if ($quantity >= parent::getMinimalAmount()) {
                $this->quantity = $quantity;
            } else {
                throw new \Exception('Item -> quantity is < Product minimal amount');
            }
        }
    }

    /**
     * Validation quantity variable
     *
     * @param $quantity
     * @return bool
     * @throws \Exception
     */
    private function validiationQuantity($quantity)
    {
        if (!empty($quantity) and is_int($quantity)) {
            if ($quantity >= 1) {
                return true;
            } else {
                throw new \Exception('Item -> quantity must be > 0');
            }
        } else {
            throw new \Exception('Item -> Invalid quantity argument');
        }
    }

    /**
     * Increasing amount
     *
     * @param integer $quantity
     * @return bool
     */
    public function addQuantity($quantity)
    {
        if ($this->validiationQuantity($quantity)) {
            $this->quantity += $quantity;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns value of product
     *
     * @return float|int
     */
    public function getValue()
    {
        return $this->quantity * $this->getPrice();
    }

    /**
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
