<?php
/**
 * Created by PhpStorm.
 * User: maksym
 * Date: 17.05.19
 * Time: 16:07
 */

namespace DealerGroup\Tests;

use DealerGroup\Item;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{

    private $productName = "string";
    private $productPrice = 1.1;
    private $testObject;

    public function setUp()
    {
        $this->testObject = new Item($this->productName, $this->productPrice, 1, 1);
    }

    /*
     * Class Construct Argument Quantity Tests
     */

    public function testClassConstructArgumentQuantityEmpty()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, null);
    }

    public function testClassConstructArgumentQuantityEmpty1()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, 0);
    }

    public function testClassConstructArgumentQuantityEmpty2()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, 0.0);
    }

    public function testClassConstructArgumentQuantityEmpty3()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, "");
    }

    public function testClassConstructArgumentQuantityEmpty4()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, "0");
    }

    public function testClassConstructArgumentQuantityEmpty5()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, array());
    }

    public function testClassConstructArgumentQuantityEmpty6()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, false);
    }

    public function testClassConstructArgumentQuantityFormat()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, 1.1);
    }

    public function testClassConstructArgumentQuantityFormat1()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, true);
    }

    public function testClassConstructArgumentQuantityFormat2()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, "string");
    }

    public function testClassConstructArgumentQuantityFormat3()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, array(1));
    }

    public function testClassConstructArgumentQuantityFormat4()
    {
        $this->expectExceptionMessage('Item -> Invalid quantity argument');
        new Item($this->productName, $this->productPrice, $this->testObject);
    }

    public function testClassConstructArgumentQuantityLogicalCondition()
    {
        $this->expectExceptionMessage('Item -> quantity must be > 0');
        new Item($this->productName, $this->productPrice, -1);
    }

    public function testClassConstructArgumentQuantityLogicalCondition2()
    {
        $this->expectExceptionMessage('Item -> quantity is < Product minimal amount');
        new Item($this->productName, $this->productPrice, 1, 2);
    }

    /*
     * Class Method Tests
     */

    public function testClassMethodsQuantityAndReturnedFormat()
    {
        $item = new Item($this->productName, $this->productPrice, 1, 1);

        $this->assertIsInt($item->getQuantity());
        $this->assertEquals($item->getQuantity(), 1);

        $this->assertIsBool($item->addQuantity(1));
        $this->assertIsInt($item->getQuantity());
        $this->assertEquals($item->getQuantity(), 2);

        $item = new Item($this->productName, $this->productPrice, 5, 5);

        $this->assertIsInt($item->getQuantity());
        $this->assertEquals($item->getQuantity(), 5);

        $this->assertEquals($item->addQuantity(5), true);

        $this->assertIsInt($item->getQuantity());
        $this->assertEquals($item->getQuantity(), 10);
    }

    public function testClassGettersMethodsProductNameAndReturnedFormat()
    {
        $item = new Item($this->productName, $this->productPrice, 1, 1);

        $this->assertIsString($item->getName());
        $this->assertEquals($item->getName(), $this->productName);

        $this->assertIsFloat($item->getPrice());
        $this->assertEquals($item->getPrice(), $this->productPrice);

        $this->assertIsInt($item->getMinimalAmount());
        $this->assertEquals($item->getMinimalAmount(), 1);
    }

    public function testClassMethodTotalValueAndReturnedFormat()
    {
        $item = new Item($this->productName, $this->productPrice, 1, 1);

        $this->assertIsFloat($item->getValue());
        $this->assertEquals($item->getValue(), $this->productPrice*1);

        $this->assertEquals($item->addQuantity(1), true);

        $this->assertIsFloat($item->getValue());
        $this->assertEquals($item->getValue(), $this->productPrice*2);

        $this->assertEquals($item->addQuantity(8), true);

        $this->assertIsFloat($item->getValue());
        $this->assertEquals($item->getValue(), $this->productPrice*10);
    }
}
