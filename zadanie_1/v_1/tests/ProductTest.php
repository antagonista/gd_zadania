<?php

namespace DealerGroup\Tests;

use DealerGroup\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /*
     * Class Construct Argument Name Tests
     */

    public function testClassConstructArgumentNameEmpty()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(null, 0.01);
    }

    public function testClassConstructArgumentNameEmpty1()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(0, 0.01);
    }

    public function testClassConstructArgumentNameEmpty2()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(0.0, 0.01);
    }

    public function testClassConstructArgumentNameEmpty3()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product("", 0.01);
    }

    public function testClassConstructArgumentNameEmpty4()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product("0", 0.01);
    }

    public function testClassConstructArgumentNameEmpty5()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(array(), 0.01);
    }

    public function testClassConstructArgumentNameEmpty6()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(false, 0.01);
    }

    public function testClassConstructArgumentNameFormat()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(1, 0.01);
    }

    public function testClassConstructArgumentNameFormat1()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(-1, 0.01);
    }

    public function testClassConstructArgumentNameFormat2()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(1.1, 0.01);
    }

    public function testClassConstructArgumentNameFormat4()
    {
        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product(array("string"), 0.01);
    }

    public function testClassConstructArgumentNameFormat5()
    {
        $testObject = new Product("test", 0.01);

        $this->expectExceptionMessage('Product -> Invalid name argument');
        new Product($testObject, 0.01);
    }

    /*
     * Class Construct Argument Price Tests
     */

    public function testClassConstructArgumentPriceEmpty()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", null);
    }

    public function testClassConstructArgumentPriceEmpty1()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", 0);
    }

    public function testClassConstructArgumentPriceEmpty2()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", 0.0);
    }

    public function testClassConstructArgumentPriceEmpty3()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", "");
    }

    public function testClassConstructArgumentPriceEmpty4()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", "0");
    }

    public function testClassConstructArgumentPriceEmpty5()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", array());
    }

    public function testClassConstructArgumentPriceEmpty6()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", false);
    }

    public function testClassConstructArgumentPriceFormat()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", true);
    }

    public function testClassConstructArgumentPriceFormat1()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", "string");
    }

    public function testClassConstructArgumentPriceFormat2()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", array(1));
    }

    public function testClassConstructArgumentPriceFormat3()
    {
        $testObject = new Product("test", 0.01);

        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", $testObject);
    }

    public function testClassConstructArgumentPriceLogicalCondition()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", -1.001);
    }

    public function testClassConstructArgumentPriceLogicalCondition1()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", -1.01);
    }

    public function testClassConstructArgumentPriceLogicalCondition2()
    {
        $this->expectExceptionMessage('Product -> Invalid price argument');
        new Product("string", -1);
    }

    public function testClassConstructArgumentPriceWrongFloatFormat()
    {
        $this->expectExceptionMessage('Product -> price is wrong float format');
        new Product("string", 1.001);
    }

    public function testClassConstructArgumentPriceWrongFloatFormat1()
    {
        $this->expectExceptionMessage('Product -> price is wrong float format');
        new Product("string", 0.001);
    }

    /*
     * Class Construct Argument Minimal Amount Tests
     */

    public function testClassConstructArgumentMinimalAmountEmpty()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1.1, 0);
    }

    public function testClassConstructArgumentMinimalAmountEmpty1()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1.1, 0.0);
    }

    public function testClassConstructArgumentMinimalAmountEmpty2()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1.1, "");
    }

    public function testClassConstructArgumentMinimalAmountEmpty3()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1.1, "0");
    }

    public function testClassConstructArgumentMinimalAmountEmpty4()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1.1, array());
    }

    public function testClassConstructArgumentMinimalAmountEmpty5()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1.1, false);
    }

    public function testClassConstructArgumentMinimalAmountFormat()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1, 1.1);
    }

    public function testClassConstructArgumentMinimalAmountFormat1()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1, true);
    }

    public function testClassConstructArgumentMinimalAmountFormat2()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1, "string");
    }

    public function testClassConstructArgumentMinimalAmountFormat3()
    {
        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1, array(1));
    }

    public function testClassConstructArgumentMinimalAmountFormat4()
    {
        $testObject = new Product("test", 0.01);

        $this->expectExceptionMessage('Product -> Invalud minimal amount argument');
        new Product("string", 1, $testObject);
    }

    public function testClassConstructArgumentMinimalAmountRequiredValue()
    {
        $this->expectExceptionMessage('Product -> minimal amount is not >= 1');
        new Product("string", 1, -1);
    }

    /*
     * Class Getters Methods
     */

    public function testClassGettersMethodsAndReturnedFormats()
    {
        $name = "string";
        $price = 1;
        $minimalAmount = 1;

        $product = new Product($name, $price);

        $this->assertEquals($product->getName(), $name);
        $this->assertIsString($product->getName());
        $this->assertEquals($product->getPrice(), $price);
        $this->assertIsFloat($product->getPrice());
        $this->assertEquals($product->getMinimalAmount(), $minimalAmount);
        $this->assertIsInt($product->getMinimalAmount());

        $minimalAmount = 2;
        $price = 0.01;

        $product = new Product($name, $price, $minimalAmount);

        $this->assertEquals($product->getName(), $name);
        $this->assertIsString($product->getName());
        $this->assertEquals($product->getPrice(), $price);
        $this->assertIsFloat($product->getPrice());
        $this->assertEquals($product->getMinimalAmount(), $minimalAmount);
        $this->assertIsInt($product->getMinimalAmount());
    }
}
